const { Schema, model } = require("mongoose");

const UsuarioSchema = Schema({
    nombre: {
        type: String,
        require: true,
    },
    password: {
        type: String,
        require: true,
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    fecha_creacion: {
        type: Date,
        require: true,
        default: new Date()
    },
    img: {
        type: String,
    },
    role: {
        type: String,
        require: true,
        default: 'USER_ROL'
    },
    estado: {
        type: Boolean,
        require: true,
        default: true
    },
    google: {
        type: Boolean,
        require: true,
        default: false
    },
});

UsuarioSchema.methods.toJSON = function(){
    const { __v, password, ...user } = this.toObject();
    return user;
}

module.exports = model( 'Usuario', UsuarioSchema );
