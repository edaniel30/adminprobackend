const { Schema, model } = require("mongoose");

const HospitalSchema = Schema({
    nombre: {
        type: String,
        require: true,
    },
    fecha_creacion: {
        type: Date,
        require: true,
        default: new Date()
    },
    img: {
        type: String,
    },
    estado: {
        type: Boolean,
        require: true,
        default: true
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        requiere: true
    }
}, { collection: 'hospitales'} );

HospitalSchema.methods.toJSON = function(){
    const { __v, ...hospital } = this.toObject();
    return hospital;
}

module.exports = model( 'Hospital', HospitalSchema );
