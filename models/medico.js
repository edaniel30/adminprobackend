const { Schema, model } = require("mongoose");

const medicoSchema = Schema({
    nombre: {
        type: String,
        require: true,
    },
    fecha_creacion: {
        type: Date,
        require: true,
        default: new Date()
    },
    img: {
        type: String,
    },
    estado: {
        type: Boolean,
        require: true,
        default: true
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        require: true
    },
    hospital: {
        type: Schema.Types.ObjectId,
        ref: 'Hospital',
        require: true
    }
});

medicoSchema.methods.toJSON = function(){
    const { __v, ...medico } = this.toObject();
    return medico;
}

module.exports = model( 'Medico', medicoSchema );
