const { response } = require("express");

const medico = require("../models/medico");

const obtenerMedicos = async ( req, res = response) => {

    try {

        const medicos = await medico.find( {estado: true} )
            .populate( 'usuario', 'nombre' )
            .populate( 'hospital' )
        
            return res.status(200).json({
            ok: true,
            medicos,
            // uid: req.uid
        });
        
    } catch (error) {
        
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });

    }

}
// ************************************************************
const crearMedico = async ( req, res = response ) => {

    try {

        const body = req.body;
        body.usuario = req.uid;
        const nuevoMedico = new medico( body );

        await nuevoMedico.save();

        return res.status(200).json({
            ok: true,
            nuevoMedico,
        });
        
    } catch (error) {
        
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });

    }
}
// *********************************************************
const editarMedico = async ( req, res = response ) => {
    try {
        const { id } = req.params;
        const body = req.body;

        const medicoEdited = await medico.findByIdAndUpdate( id, body, {new: true} );

        return res.status(200).json({
            ok: true,
            medicoEdited
        });
        
    } catch (error) {

        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });
        
    }
}
// *********************************************************
const eliminarMedico = async ( req, res = response ) => {
    try {

        const { id } = req.params;
        const medicoDeleted = await medico.findByIdAndUpdate( id, {estado: false} );

        return res.status(200).json({
            ok: true,
            id
        });
        
    } catch (error) {

        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });
        
    }
}


module.exports = {
    obtenerMedicos,
    crearMedico,
    editarMedico,
    eliminarMedico
}