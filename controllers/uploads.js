
const path = require('path');
const { response } = require("express");
const { v4: uuidv4 } = require('uuid');
const { actualizarImagen } = require("../helpers/actualizar-imagen");

const cargarArchivo = async ( req, res = response ) => {

    try {

        const { tipo, id } = req.params;

        // validar tipo
        const tiposValidos = ['hospitales', 'medicos', 'usuarios']

        if ( !tiposValidos.includes( tipo ) ){
            return res.status(400).json({
                ok: true,
                msg: 'la colección no es valida'
            });
        }

        const file = req.files.archivo;
        const nombreCortado = file.name.split('.');
        // console.log(nombreCortado);
        const extensionArchivo = nombreCortado[ nombreCortado.length - 1 ];
        // console.log(extensionArchivo);

        // validar extension
        const extValidas = ['png', 'jpg'];

        if ( !extValidas.includes( extensionArchivo ) ){
            return res.status(400).json({
                ok: true,
                msg: 'la extensión no es valida'
            });
        }

        // nombre de archivo
        const nombreArchivo = `${uuidv4()}.${extensionArchivo}`

        // path para guarada imagen
        const path = `./uploads/${tipo}/${nombreArchivo}`

        // movel la imagen
        file.mv( path, (err) =>{
            if (err){
                return res.status(200).json({
                    ok: true,
                    msg: 'error al mover la imagen'
                });
            }

            // actualizar en DB
            actualizarImagen( tipo, id, nombreArchivo );

            return res.status(200).json({
                ok: true,
                nombreArchivo
            });
        })



        
    } catch (error) {
        
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });

    }
}

const retornaImg = ( req, res = response ) => {

    const { tipo, foto } = req.params;

    const pathImg = path.join( __dirname, `../uploads/${tipo}/${foto}`);

    res.sendFile( pathImg );
}

module.exports = {
    cargarArchivo,
    retornaImg
}