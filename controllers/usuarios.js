const { response } = require("express");
const bcryptjs = require('bcryptjs');
const { generarJWT } = require('../helpers/jwt');

const usuario = require("../models/usuario");

const obtenerUsuarios = async ( req, res = response ) => {

    try {

        const desde = Number(req.query.desde) || 0;
        // const hasta = Number(req.query.hasta) || ;
        console.log(desde);

         const [usuarios, total] = await Promise.all([
            usuario.find( {estado: true} )
                .skip( desde )
                .limit( 5 ),
            usuario.count()
        ])

        res.status(200).json({
            ok: true,
            usuarios,
            uid: req.uid,
            total
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });
    }

}
// *********************************************************
const crearUsuario = async ( req, res = response ) => {
    try {

        const {email, password, nombre} = req.body;
        const nuevoUser = new usuario( {email, password, nombre} );

        // Encriptar la contraseña
        const salt = bcryptjs.genSaltSync();
        nuevoUser.password = bcryptjs.hashSync( password, salt );


        await nuevoUser.save();

        // Generar JWT
        const token = await generarJWT( nuevoUser._id, nuevoUser.nombre );

        return res.status(200).json({
            ok: true,
            nuevoUser,
            token,
        });
        
    } catch (error) {

        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });
        
    }
}
// *********************************************************
const editarUsuario = async ( req, res = response ) => {
    try {
        const { id } = req.params;
        const {password, google, email, ...body} = req.body;

        const userDB = await usuario.findById( id );

        if ( email !== userDB.email ){

            const userEmail = await usuario.findOne( {email} )
            if ( userEmail ){
                return res.status(400).json({
                    ok: false,
                    msg: 'Ya existe un usuario con ese email'
                });
            }
            
        }

        body.email = email;
        
        const usuarioEdited = await usuario.findByIdAndUpdate( id, body, {new: true} );

        return res.status(200).json({
            ok: true,
            usuarioEdited
        });
        
    } catch (error) {

        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });
        
    }
}
// *********************************************************
const eliminarUsuario = async ( req, res = response ) => {
    try {

        const { id } = req.params;
        const userEliminated = await usuario.findByIdAndUpdate( id, {estado: false} );


        return res.status(200).json({
            ok: true,
            id
        });
        
    } catch (error) {

        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });
        
    }
}

module.exports = {
    obtenerUsuarios,
    crearUsuario,
    editarUsuario,
    eliminarUsuario
}