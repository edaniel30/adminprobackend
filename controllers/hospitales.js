const { response } = require("express");

const hospital = require("../models/hospital");

const obtenerHospitales = async ( req, res = response) => {

    try {

        const hospitales = await hospital.find( {estado: true} )
            .populate( 'usuario', 'nombre' )
        
        res.status(200).json({
            ok: true,
            hospitales,
            // uid: req.uid
        });
        
    } catch (error) {
        
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });

    }

}
// ************************************************************
const crearHospital = async ( req, res = response ) => {

    try {

        const body = req.body;
        body.usuario = req.uid;
        const nuevoHospital = new hospital( body );

        await nuevoHospital.save();

        return res.status(200).json({
            ok: true,
            nuevoHospital,
        });
        
    } catch (error) {
        
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });

    }
}
// *********************************************************
const editarHospital = async ( req, res = response ) => {
    try {
        const { id } = req.params;
        const body = req.body;

        const hospitalEdited = await hospital.findByIdAndUpdate( id, body, {new: true} );

        return res.status(200).json({
            ok: true,
            hospitalEdited
        });
        
    } catch (error) {

        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });
        
    }
}
// *********************************************************
const eliminarHospital = async ( req, res = response ) => {
    try {

        const { id } = req.params;
        const hospitalDeleted = await hospital.findByIdAndUpdate( id, {estado: false} );

        return res.status(200).json({
            ok: true,
            id
        });
        
    } catch (error) {

        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });
        
    }
}


module.exports = {
    obtenerHospitales,
    crearHospital,
    editarHospital,
    eliminarHospital
}