const { response } = require('express');
const bcryptjs = require('bcryptjs');
const { generarJWT } = require('../helpers/jwt');

const Usuario = require('../models/usuario');
const { googleVerify } = require('../helpers/google-verify');
const usuario = require('../models/usuario');


// Ingreso a la plataforma
const login = async (req, res = response) => {

    const { email, password } = req.body;

    try {
        
        // Verificar si el email existe
        const usuario = await Usuario.findOne({ email });

        if ( !usuario ){
            return res.status(400).json({
                msg: 'Credenciales no validas'
            });
        }

        // Verificar si el usuario esta activo
        if ( !usuario.estado ){
            return res.status(400).json({
                ok: false,
                msg: 'Usuario inactivo'
            });
        }

        // Verificar la contraseña
        const validPassword = bcryptjs.compareSync( password, usuario.password );
        if ( !validPassword ){
            return res.status(400).json({
                ok:false,
                msg: 'Credenciales no validas'
            });
        }

        // Generar JWT
        const token = await generarJWT( usuario.id );

        return res.json({
            ok:true,
            usuario,
            token
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            msg:'Comuniquese con el administrador'
        });
    }

    
}
// **************************************************
const googleSingIn = async ( req, res = response ) => {

    const googleToken = req.body.token;

    try {

        const { name, email, picture } = await googleVerify( googleToken );

        const userDB = await usuario.findOne( {email} );
        let user;

        if ( !userDB ){
            // si no existe el useuario
            user = new usuario({
                nombre: name,
                email,
                password: '@@@',
                img: picture,
                google: true
            });
        } else{
            // si existe usuario
            user = userDB;
            user.google = true;
            usuario.password = '@@@';
        }

        // guardar en DB
        await user.save();

        // Generar JWT
        const token = await generarJWT( usuario.id );
        
        return res.status(200).json({
            ok: true,
            msg: 'Todo OK',
            token
        })

    } catch (error) {
        
        return res.status(401).json({
            ok: false,
            msg: 'Token incorrecto'
        })
    }


}
// **************************************************
const renewToken = async ( req, res = response ) => {

    try {

        const uid = req.uid;

        // Generar JWT
        const token = await generarJWT( uid );

        return res.status(200).json({
            ok: true,
            token
        })

    } catch (error) {
        
        return res.status(401).json({
            ok: false,
            msg: 'Internal error'
        })
    }


}


module.exports = {
    login,
    googleSingIn,
    renewToken
}