const { response } = require("express");

const usuario = require("../models/usuario");
const medico = require("../models/medico");
const hospital = require("../models/hospital");

const buscar = async ( req, res = response ) => {

    try {

        const { busqueda } = req.params;
        const regex = new RegExp( busqueda, 'i');

        const [ infoUsuario, infoHospitales, infoMedico ] = await Promise.all([
            usuario.find( { nombre: regex} ),
            hospital.find( { nombre: regex} ),
            medico.find( { nombre: regex} )
        ])

        return res.status(200).json({
            ok: true,
            busqueda,
            infoUsuario,
            infoHospitales,
            infoMedico
            // uid: req.uid
        });

        
    } catch (error) {
        
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });

    }
}
// *************************************************
const buscarColeccion = async ( req, res = response ) => {

    try {

        const { busqueda, tabla } = req.params;
        const regex = new RegExp( busqueda, 'i');

        switch (tabla) {
            case 'usuario':
                info = await usuario.find( { nombre: regex} );
                break;
            case 'hospital':
                info = await hospital.find( { nombre: regex} );
                break;
            case 'medico':
                info = await medico.find( { nombre: regex} );
                break;
            default:
                res.status(400).json({
                    ok: false,
                    msg: 'colección invalida'
                });
            break;
        }

        return res.status(200).json({
            ok: true,
            busqueda,
            info
        });

        

        
    } catch (error) {
        
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Internal error'
        });

    }
}

module.exports = {
    buscar,
    buscarColeccion
}