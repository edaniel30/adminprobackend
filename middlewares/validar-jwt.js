const { response, request } = require('express');
const jwt = require('jsonwebtoken');
const usuario = require('../models/usuario');

const validarJWT = async ( req = request, res = response, next) => {

    const token = req.header('x-token');

    if ( !token ){
        return res.status(401).json({
            ok: false,
            msg: 'No hay token en la petición'
        });
    }

    try {

        const { uid } = jwt.verify( token, process.env.SECRETORPRIVATEKEY );

        // Leer useuario qeu corresponde al uid
        const user = await usuario.findById( uid );
        if ( !user ){
            res.status(401).json({
                ok: false,
                msg: 'Token no valido - user is NULL'
            });
        }

        // Verificar si el uid no esta eliminado
        if ( !user.estado ){
            res.status(401).json({
                ok: false,
                msg: 'Token no valido - user inactivo'
            });
        }
        // console.log(user);
        req.uid = uid;

        next();

    } catch (error) {
        console.log(error);
        res.status(401).json({
            ok: false,
            msg: 'Token no valido'
        });
    }

    // console.log(token);


}

module.exports = {
    validarJWT,
}
