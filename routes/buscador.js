const { Router } = require("express");
const { check } = require('express-validator');
const { buscar, buscarColeccion } = require("../controllers/busqueda");
const { validarJWT } = require("../middlewares/validar-jwt");

const router = Router();

router.get('/:busqueda/', [
    validarJWT
], buscar)

router.get('/coleccion/:tabla/:busqueda/', [
    validarJWT
], buscarColeccion)

module.exports = router;