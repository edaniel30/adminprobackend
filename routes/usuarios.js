const { Router } = require("express");
const { check } = require('express-validator');

const { validarCampos } = require('../middlewares/validar-campos');
const { existeEmail, existeUsuarioId } = require("../helpers/db-validators");
const { obtenerUsuarios, 
        crearUsuario, 
        editarUsuario, 
        eliminarUsuario} = require("../controllers/usuarios");
const { validarJWT } = require("../middlewares/validar-jwt");


const router = Router();


router.get('/', [
    validarJWT
], obtenerUsuarios);

router.post('/', [
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'La contraseña es obligatoria').not().isEmpty(),
    check('email').custom( existeEmail ),
    check('password', 'El password es obligatorio').not().isEmpty(),
    validarCampos
], crearUsuario);

router.put('/:id', [
    validarJWT,
    check('id', 'El id no es valido').isMongoId(),
    check('id').custom( existeUsuarioId ),
    validarCampos
], editarUsuario);

router.delete('/:id', [
    validarJWT,
    check('id', 'El id no es valido').isMongoId(),
    check('id').custom( existeUsuarioId ),
    validarCampos
], eliminarUsuario);


module.exports = router;