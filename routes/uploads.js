const { Router } = require("express");
const { cargarArchivo, retornaImg } = require("../controllers/uploads");
const { validarJWT } = require("../middlewares/validar-jwt");

const expressFileUpload = require("express-fileupload"); 
const { validarArchivo } = require("../middlewares/validar-archivos");

const router = Router();

router.use( expressFileUpload());

router.put('/:tipo/:id', [
    validarJWT,
    validarArchivo
], cargarArchivo)

router.get('/:tipo/:foto', [
], retornaImg)

module.exports = router;