const { Router } = require("express");
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require("../middlewares/validar-jwt");

const { crearHospital, 
        obtenerHospitales, 
        editarHospital,
        eliminarHospital} = require("../controllers/hospitales");

const { existeHospitalId } = require("../helpers/db-validators");

const router = Router();

router.get('/', [
    // validarJWT
], obtenerHospitales);

router.post('/', [
    validarJWT,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    validarCampos
], crearHospital);

router.put('/:id', [
    validarJWT,
    check('id', 'El id no es valido').isMongoId(),
    check('id').custom( existeHospitalId ),
    validarCampos
], editarHospital);

router.delete('/:id', [
    validarJWT,
    check('id', 'El id no es valido').isMongoId(),
    check('id').custom( existeHospitalId ),
    validarCampos
], eliminarHospital);


module.exports = router;