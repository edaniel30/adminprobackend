const { Router } = require("express");
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require("../middlewares/validar-jwt");

const { obtenerMedicos, crearMedico, editarMedico, eliminarMedico } = require("../controllers/medicos");
const { existeHospitalId, existeUsuarioId, existeMedicoId } = require("../helpers/db-validators");

const router = Router();

router.get('/', [
    // validarJWT
], obtenerMedicos);

router.post('/', [
    validarJWT,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('hospital', 'El id de hospital no es valido').isMongoId(),
    check('hospital').custom( existeHospitalId ),
    validarCampos
], crearMedico);

router.put('/:id', [
    validarJWT,
    check('id', 'El id no es valido').isMongoId(),
    check('id').custom( existeMedicoId ),
    validarCampos
], editarMedico);

router.delete('/:id', [
    validarJWT,
    check('id', 'El id no es valido').isMongoId(),
    check('id').custom( existeMedicoId ),
    validarCampos
], eliminarMedico);


module.exports = router;