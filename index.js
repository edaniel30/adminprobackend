const { response } = require('express');
require('dotenv').config();

const express = require('express');
const { dbConnection } = require('./db/config');
const cors = require('cors')

// crear servidor de express
const app = express();

// conectar con DB
dbConnection();

// directorio publico
app.use( express.static('public') );

// Cors
app.use(cors()); 

// lectura y parseo del body
app.use( express.json() );

// Rutas
app.use( '/api/usuarios', require( './routes/usuarios' ) );
app.use( '/api/login', require( './routes/auth' ) );
app.use( '/api/hospitales', require( './routes/hospitales' ) );
app.use( '/api/medicos', require( './routes/medicos' ) );
app.use( '/api/buscador', require( './routes/buscador' ) );
app.use( '/api/uploads', require( './routes/uploads' ) );

app.listen( process.env.PORT, () => {
    console.log(`servidor corriendo en puerto: ${process.env.PORT}`);
});