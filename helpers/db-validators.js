
const hospital = require("../models/hospital");
const medico = require("../models/medico");
const usuario = require("../models/usuario");


const existeEmail = async ( email ) => {

    const userEmail = await usuario.findOne( {email} )
    if ( userEmail ){
        throw new Error( `El correo: ${email} ya está registrado.` )
    }

}
// ************************************************************************
const existeUsuarioId = async ( userId ) => {

    const user = await usuario.findById( userId )
    if ( !user ){
        throw new Error( `No existe usuario con id: ${userId}.` )
    }

}
// ************************************************************************
const existeHospitalId = async ( hospitalId ) => {

    const hosptl = await hospital.findById( hospitalId )
    if ( !hosptl ){
        throw new Error( `No existe hospital con id: ${hospitalId}.` )
    }

}
// ************************************************************************
const existeMedicoId = async ( medicoId ) => {

    const medi = await medico.findById( medicoId )
    if ( !medi ){
        throw new Error( `No existe medico con id: ${medicoId}.` )
    }

}

module.exports = {
    existeEmail,
    existeUsuarioId,
    existeHospitalId,
    existeMedicoId,
}