const fs = require('fs');

const usuario = require("../models/usuario");
const medico = require("../models/medico");
const hospital = require("../models/hospital");


const borrarImagne = async ( path ) => {

    if ( fs.existsSync( path ) ){
        fs.unlinkSync( path );
    }
}
// ***********************************************
const actualizarImagen = async ( tipo, id, nombreArchivo  ) => {

    switch ( tipo ) {
        case 'medicos':
            const medicoActual = await medico.findById( id )

            if ( !medicoActual ){
                return false;
            } 

            pahtViejo = `./uploads/medicos/${medicoActual.img}`
            borrarImagne( pahtViejo );
            
            medicoActual.img = nombreArchivo;

            await medicoActual.save();
            return true;

        case 'usuarios':
            const user = await usuario.findById( id )

            if ( !user ){
                return false;
            } 

            pahtViejo = `./uploads/usuarios/${user.img}`
            borrarImagne( pahtViejo );
            
            user.img = nombreArchivo;

            await user.save();
            return true;

        case 'hospitales':
            const hosp = await hospital.findById( id )

            if ( !hosp ){
                return false;
            } 

            pahtViejo = `./uploads/hospitales/${hosp.img}`
            await borrarImagne( pahtViejo );
            
            hosp.img = nombreArchivo;

            await hosp.save();
            return true;
        
        break;
    
        default:
            break;
    }

}

module.exports = {
    actualizarImagen
}