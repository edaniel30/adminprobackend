const jwt = require('jsonwebtoken');

const generarJWT = ( uid ) => {

    const payload = {
        uid, 
    };

    return new Promise( (resolve, reject) => {

        jwt.sign( payload, process.env.SECRETORPRIVATEKEY, {
            expiresIn: '12h',
        }, (err, token) => {
            if (err){
                // Todo mal
                console.log(err);
                reject(err);
            } else {
                // Todo bien
                resolve(token);
            }
        });

    });

}

module.exports = {
    generarJWT
}

